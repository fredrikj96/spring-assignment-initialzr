package scania.reskilling.fredrik.springinitialzrassignment1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringInitialzrAssignment1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringInitialzrAssignment1Application.class, args);
    }

}
