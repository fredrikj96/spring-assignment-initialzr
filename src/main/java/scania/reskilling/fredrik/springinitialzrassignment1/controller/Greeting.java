package scania.reskilling.fredrik.springinitialzrassignment1.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class Greeting {

    @GetMapping("/greeting")

    public String basicGreeting () {
        return "All hail the almighty ruler, superior leader of just and right, divine creature of sacred sexual allure and charisma, Fredrik.";
    }

    @GetMapping("/greeting/{id}")
    public String nameSpecificGreeting(@PathVariable String id) {
        return "All hail the almighty ruler, superior leader of just and right, divine creature of sacred sexual allure and charisma, " + id +".";

    }

    @GetMapping("/palindrome")
    public String isItPaliOrNot(@RequestParam ("query") String inputPali) {

        String paliRev = new StringBuffer(inputPali).reverse().toString();

        if (inputPali.equals(paliRev))
        {
            return "It's a palindrome, cool!";
        }
        else
            return "It's not a palindrome, try with another word.";
    }

}
